package entity

import (
	"gitlab.com/konstellation/kre/k8s-manager/proto/runtimepb"
)

type Runtime struct {
	runtimepb.Runtime
}
