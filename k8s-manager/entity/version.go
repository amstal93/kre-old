package entity

import (
	"gitlab.com/konstellation/kre/k8s-manager/proto/versionpb"
)

type Version struct {
	versionpb.Version
}
