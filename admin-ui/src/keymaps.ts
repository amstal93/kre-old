const keymaps = {
  OPEN_LOGS: 'alt+l',
  CLOSE_LOGS: 'esc'
};

export default keymaps;
