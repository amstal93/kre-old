/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GetAllowedEmails
// ====================================================

export interface GetAllowedEmails_settings {
  __typename: 'Settings';
  authAllowedEmails: string[];
}

export interface GetAllowedEmails {
  settings: GetAllowedEmails_settings;
}
