/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GetUserEmail
// ====================================================

export interface GetUserEmail_me {
  __typename: 'User';
  email: string;
}

export interface GetUserEmail {
  me: GetUserEmail_me | null;
}
