module greeter

go 1.14

require gitlab.com/konstellation/kre/runtime-runners/kre-go v0.0.0-20200520085043-c6c388dc28d4

replace gitlab.com/konstellation/kre/runtime-runners/kre-go => ../../../../runtime-runners/kre-go
