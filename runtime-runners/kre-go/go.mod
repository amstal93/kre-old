module gitlab.com/konstellation/kre/runtime-runners/kre-go

go 1.14

require (
	github.com/golang/protobuf v1.4.2 // indirect
	github.com/nats-io/nats-server/v2 v2.1.7 // indirect
	github.com/nats-io/nats.go v1.10.0
	gitlab.com/konstellation/kre/libs/simplelogger v0.0.0-20200520142015-e16ab4558d6e
)
