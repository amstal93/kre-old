module gitlab.com/konstellation/kre/runtime-api

go 1.13

require (
	github.com/gogo/protobuf v1.0.0 // indirect
	github.com/golang/protobuf v1.3.2
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/labstack/gommon v0.3.0
	github.com/stretchr/testify v1.4.0
	github.com/xdg/stringprep v1.0.0 // indirect
	gitlab.com/konstellation/kre/libs/simplelogger v0.0.0-20200227100435-816f3595dc29
	go.mongodb.org/mongo-driver v1.3.0
	google.golang.org/grpc v1.27.1
	gopkg.in/yaml.v2 v2.2.7
	k8s.io/api v0.0.0-20190918195907-bd6ac527cfd2
	k8s.io/apimachinery v0.0.0-20190817020851-f2f3a405f61d
	k8s.io/client-go v0.0.0-20190918200256-06eb1244587a
)
